package vt.cs.onlinetool.basicactions;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import com.google.apphosting.api.DeadlineExceededException;

import AST.ParseException;

public class TestAnalyzer {

	@Test
	public void testAnalysis1() throws IOException {
		String url = "https://scratch.mit.edu/projects/154605937/";
		String pattern = "(\\d+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(url);
		String projectSrc = "";
		if(m.find()){
			projectSrc = Util.retrieveProjectOnline(m.group());
		}
		
		String result;
		try {
			result = Util.analyze(projectSrc);
		} catch (ParseException e) {
			e.printStackTrace();
			result = "we can't handle it yet";
		}catch (DeadlineExceededException deadLineException){
			result = "{}";
		} catch (Exception e) {
			result = "{}";
		} 
	}

}
