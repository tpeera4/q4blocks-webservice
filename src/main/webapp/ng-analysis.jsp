<%@ page language="java" contentType="text/html; charset=utf-8"
  pageEncoding="utf-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <html lang="en">
    <head>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
      <link href="static/css/bootstrap.min.css" rel="stylesheet"><!-- Custom CSS -->
        <link href="static/css/analysis-scrolling-nav.css" rel="stylesheet">
        <style>
        /*div.smell-section {height: 500px;}*/
        </style>
        <!-- <link href="css/scrolling-nav.css" rel="stylesheet"> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
        </script>
      </head>
      <body>
        <div class="container" ng-app="analysis-result" ng-controller="customersCtrl">
          <div class="row">


            <div class="col-sm-9">
               <div ng-repeat="scriptable in scriptables">
                  <div id={{scriptable.scriptable}} class="smell-section" ng-cloak>
                     <h1>{{scriptable.scriptable}}</h1>
                     <li ng-repeat="(smell_name, instances) in scriptable.instances">
                        {{smell_name}}
                        <ul>
                           <li ng-repeat="instance in instances track by $index">
                              <div ng-if="!['Duplicated Code','Uncommunicative Name'].includes(smell_name)">
                                 <div svg scratchblocks={{instance}}></div>
                              </div>
                              <div ng-if="smell_name == 'Duplicated Code'">
                                 <h4>Clone group {{$index}}</h4>
                                 <ul>
                                    <li ng-repeat="clone in instance track by $index">
                                       <div svg scratchblocks={{clone}}></div>
                                    </li>
                                 </ul>
                              </div>
                              <div ng-if="smell_name == 'Uncommunicative Name'">
                                 <h4>{{instance}}</h4>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </div>
               </div>
            </div>



              </div>
            </div>
            <script>
            var app = angular.module('analysis-result', []);
            app.controller('customersCtrl', function($scope, $http) {
              $scope.scriptables = ${scriptables};
              $scope.formatBlockSeq = function(blocks){
                var result = "";
                for(var i=0; i<blocks.length;i++){
                  result += blocks[i];
                  result += "\n";
                }
                return result;
              };

            });

            app.directive("svg",function(){
              return {
                link: function (scope, iElement, iAttrs) {
                  script = "move (10) steps";
                  var svg = scratchblocks(iAttrs.scratchblocks);
                  iElement.append(svg);
                }

              }
            });


            </script>

            <defs>
              <filter height="200%" id="bevelFilter" width="200%" x0="-50%" y0="-50%">
                <fegaussianblur in="SourceAlpha" result="blur-1" stddeviation="1 1"></fegaussianblur>
                <feflood flood-color="#fff" flood-opacity="0.15" in="undefined" result="flood-2"></feflood>
                <feoffset dx="1" dy="1" in="blur-1" result="offset-3"></feoffset>
                <fecomposite in="SourceAlpha" in2="offset-3" k2="1" k3="-1" operator="arithmetic" result="comp-4"></fecomposite>
                <fecomposite in="flood-2" in2="comp-4" operator="in" result="comp-5"></fecomposite>
                <feflood flood-color="#000" flood-opacity="0.7" in="undefined" result="flood-6"></feflood>
                <feoffset dx="-1" dy="-1" in="blur-1" result="offset-7"></feoffset>
                <fecomposite in="SourceAlpha" in2="offset-7" k2="1" k3="-1" operator="arithmetic" result="comp-8"></fecomposite>
                <fecomposite in="flood-6" in2="comp-8" operator="in" result="comp-9"></fecomposite>
                <femerge result="merge-10">
                  <femergenode in="SourceGraphic"></femergenode>
                  <femergenode in="comp-5"></femergenode>
                  <femergenode in="comp-9"></femergenode>
                </femerge>
              </filter>
              <filter height="200%" id="inputBevelFilter" width="200%" x0="-50%" y0="-50%">
                <fegaussianblur in="SourceAlpha" result="blur-1" stddeviation="1 1"></fegaussianblur>
                <feflood flood-color="#fff" flood-opacity="0.15" in="undefined" result="flood-2"></feflood>
                <feoffset dx="-1" dy="-1" in="blur-1" result="offset-3"></feoffset>
                <fecomposite in="SourceAlpha" in2="offset-3" k2="1" k3="-1" operator="arithmetic" result="comp-4"></fecomposite>
                <fecomposite in="flood-2" in2="comp-4" operator="in" result="comp-5"></fecomposite>
                <feflood flood-color="#000" flood-opacity="0.7" in="undefined" result="flood-6"></feflood>
                <feoffset dx="1" dy="1" in="blur-1" result="offset-7"></feoffset>
                <fecomposite in="SourceAlpha" in2="offset-7" k2="1" k3="-1" operator="arithmetic" result="comp-8"></fecomposite>
                <fecomposite in="flood-6" in2="comp-8" operator="in" result="comp-9"></fecomposite>
                <femerge result="merge-10">
                  <femergenode in="SourceGraphic"></femergenode>
                  <femergenode in="comp-5"></femergenode>
                  <femergenode in="comp-9"></femergenode>
                </femerge>
              </filter>
              <filter height="200%" id="inputDarkFilter" width="200%" x0="-50%" y0="-50%">
                <feflood flood-color="#000" flood-opacity="0.2" in="undefined" result="flood-1"></feflood>
                <fecomposite in="flood-1" in2="SourceAlpha" operator="in" result="comp-2"></fecomposite>
                <femerge result="merge-3">
                  <femergenode in="SourceGraphic"></femergenode>
                  <femergenode in="comp-2"></femergenode>
                </femerge>
              </filter>
              <path d="M1.504 21L0 19.493 4.567 0h1.948l-.5 2.418s1.002-.502 3.006 0c2.006.503 3.008 2.01 6.517 2.01 3.508 0 4.463-.545 4.463-.545l-.823 9.892s-2.137 1.005-5.144.696c-3.007-.307-3.007-2.007-6.014-2.51-3.008-.502-4.512.503-4.512.503L1.504 21z" fill="#3f8d15" id="greenFlag"></path>
              <path d="M6.724 0C3.01 0 0 2.91 0 6.5c0 2.316 1.253 4.35 3.14 5.5H5.17v-1.256C3.364 10.126 2.07 8.46 2.07 6.5 2.07 4.015 4.152 2 6.723 2c1.14 0 2.184.396 2.993 1.053L8.31 4.13c-.45.344-.398.826.11 1.08L15 8.5 13.858.992c-.083-.547-.514-.714-.963-.37l-1.532 1.172A6.825 6.825 0 0 0 6.723 0z" fill="#fff" id="turnRight"></path>
              <path d="M3.637 1.794A6.825 6.825 0 0 1 8.277 0C11.99 0 15 2.91 15 6.5c0 2.316-1.253 4.35-3.14 5.5H9.83v-1.256c1.808-.618 3.103-2.285 3.103-4.244 0-2.485-2.083-4.5-4.654-4.5-1.14 0-2.184.396-2.993 1.053L6.69 4.13c.45.344.398.826-.11 1.08L0 8.5 1.142.992c.083-.547.514-.714.963-.37l1.532 1.172z" fill="#fff" id="turnLeft"></path>
              <path d="M0 0L4 4L0 8Z" fill="#111" id="addInput"></path>
              <path d="M4 0L4 8L0 4Z" fill="#111" id="delInput"></path>
              <g id="loopArrow">
                <path d="M8 0l2 -2l0 -3l3 0l-4 -5l-4 5l3 0l0 3l-8 0l0 2" fill="#000" opacity="0.3"></path>
                <path d="M8 0l2 -2l0 -3l3 0l-4 -5l-4 5l3 0l0 3l-8 0l0 2" fill="#fff" opacity="0.9" transform="translate(-1 -1)"></path>
              </g>
            </defs>
            <g></g></svg></pre>
            <script src="static/js/scratchblocks.js"></script>

          </body>
        </html>
        <!-- [END base]-->
