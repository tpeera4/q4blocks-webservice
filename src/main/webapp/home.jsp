<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<link href="/static/css/bootstrap.min.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="/static/css/scrolling-nav.css" rel="stylesheet"><!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<title>Scratch Analyzer</title>
  </head>
  <body data-spy="scroll" data-target=".navbar-fixed-top" id="page-top" data-offset="50">
  	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header page-scroll">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand page-scroll" href="#page-top">Smell Analyzer</a>
			</div><!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden">
						<a class="page-scroll" href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="#about">About</a>
					</li>
					
					<li>
						<a class="page-scroll" href="#contact">Contact</a>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container -->
	</nav>
	
		<section class="intro-section" id="intro">
    <div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 hid">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3 text-center">
							<h1>Code Quality Analysis</h1>
								<form method="POST" action="${destination}">
								<div class="form-group">
									<input class="form-control" id="url" name="URL" placeholder="Scratch Project URL" type="text" value="">
								</div><button class="btn btn-default" type="submit">Analyze</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- About Section -->
	<section class="about-section" id="about">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1>About</h1>
					<p>Automated code smell analysis tool for Scratch</p>
				</div>
			</div>
		</div>
	</section><!-- Analysis Section -->

<!-- Contact Section -->
	<section class="contact-section" id="contact">
		<div class="container">
			<div class="row">
				<h1>Contact</h1>
				<!--<p>	Peeratham Techapalokul, Graduate Student <br>
				Email: tpeera4@cs.vt.edu	</p>
				<p> Faculty Supervisor: Eli Tilevich, Ph.D. <br>
				Software Innovations Lab </p>
				Department of Computer Science, Virginia Tech
				-->
			</div>

		</div>
	</section>
	
	
	<script src="/static/js/jquery.js">
	</script> <!-- Bootstrap Core JavaScript -->

	<script src="/static/js/bootstrap.min.js">
	</script> <!-- Scrolling Nav JavaScript -->

	<script src="/static/js/jquery.easing.min.js">
	</script>
	<script src="/static/js/scrolling-nav.js">
	</script>
  </body>
</html>
<!-- [END base]-->
