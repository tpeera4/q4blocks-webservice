package vt.cs.onlinetool.basicactions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.apphosting.api.DeadlineExceededException;

import AST.ParseException;

@SuppressWarnings("serial")
public class AnalysisServiceServlet extends HttpServlet {
	Logger logger = Logger.getLogger("AnalysisService");
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		resp.setContentType("application/json");
		
		String url = req.getParameter("URL").toString();
		String pattern = "(\\d+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(url);
		String projectSrc = "";
		if(m.find()){
			projectSrc = Util.retrieveProjectOnline(m.group());
		}else{
			throw new ServletException("Invalid Scratch Project URL");
		}
		
		String result;
		try {
			result = Util.analyze(projectSrc);
		} catch (ParseException e) {
			e.printStackTrace();
			result = "we can't handle it yet";
		}catch (DeadlineExceededException deadLineException){
			logger.log(Level.SEVERE, "Exceeded time limit");
			result = "{}";
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			result = "{}";
		} 
		
		resp.addHeader("Access-Control-Allow-Origin", "*");
//		resp.addHeader("Access-Control-Allow-Origin", "http://localhost:1313");
//		resp.addHeader("Access-Control-Allow-Origin", "http://research.cs.vt.edu");
		resp.getWriter().write(result);
		
	}

}
