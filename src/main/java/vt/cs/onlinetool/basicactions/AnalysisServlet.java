package vt.cs.onlinetool.basicactions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.AnalysisManager;
import main.ScratchParser;

import org.jsoup.Jsoup;

import com.google.apphosting.api.DeadlineExceededException;

import AST.ParseException;
import AST.Program;

@SuppressWarnings("serial")
public class AnalysisServlet extends HttpServlet {
	Logger logger = Logger.getLogger("Analysis");

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		// Assuming your json object is **jsonObject**, perform the following,
		// it will return your json object
//		String result;
//		try {
//			result = analyze();
//		} catch (ParseException e) {
//			result = "we can't handle it yet";
//		}
//		out.print(result);
//		req.setAttribute("result", result);
//		out.flush();
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = req.getParameter("URL").toString();
		String pattern = "(\\d+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(url);
		String projectSrc = "";
		if(m.find()){
			projectSrc = Util.retrieveProjectOnline(m.group());
		}else{
			throw new ServletException("Invalid Scratch Project URL");
		}
		
		String result;
		try {
			result = Util.analyze(projectSrc);
		} catch (ParseException e) {
			e.printStackTrace();
			result = "we can't handle it yet";
		}catch (DeadlineExceededException deadLineException){
			logger.log(Level.SEVERE, "Exceeded time limit");
			result = "{}";
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			result = "{}";
		} 
		
		req.setAttribute("scriptables", result);
		req.getRequestDispatcher("/ng-analysis.jsp").forward(req, resp);
	}

	

	

}
