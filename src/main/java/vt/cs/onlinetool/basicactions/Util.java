package vt.cs.onlinetool.basicactions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import org.jsoup.Jsoup;

import AST.Program;
import main.AnalysisManager;
import main.ScratchParser;

public class Util {
	public static String analyze(String projectSrc) throws Exception {
		StringReader srd;
		srd = new StringReader(projectSrc);

		Program parseTree;
		ScratchParser parser = new ScratchParser();
		parseTree = parser.parse(srd);

		String result = AnalysisManager.process(parseTree);
		return result;
	}
	
	public static String retrieveProjectOnline(String string)
			throws IOException {
		String baseDownLoadURL = "http://projects.scratch.mit.edu/internalapi/project/%1$s/get/";
		String URL = String.format(baseDownLoadURL, string);
		String doc = Jsoup.connect(URL).ignoreContentType(true).execute()
				.body();
		return doc;
	}
	
	public static String getProjectSourceFromFile() {
		InputStream is = Util.class.getResourceAsStream("/input3.txt");
		BufferedReader br = null;
		StringReader srd;
		try {
			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String inputLine = null;
		StringBuilder builder = new StringBuilder();

		try {
			while ((inputLine = br.readLine()) != null)
				builder.append(inputLine);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
}
