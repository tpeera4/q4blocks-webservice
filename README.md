# Documentation
##### Local testing
```sh
mvn appengine:devserver
```

##### To update and deploy
```sh
mvn clean appengine:update
```
RES API call
http://localhost:8080/analysis-service?URL=https://scratch.mit.edu/projects/154605937/
https://scratchanalyzer.appspot.com/analysis-service?URL=https://scratch.mit.edu/projects/154605937/

-----------------------------

## Todo
- [ ] cache result in a datastore
- [ ] progress update
------------
## Done
- [x] a REST webservice Example: REST API call
- [x] fix AnalysisTool bug


## Temp
[ ] AppEngine: Input JSON script , output ScratchBlocks
	* https://github.com/GoogleCloudPlatform/appengine-java-vm-hello
	* https://github.com/GoogleCloudPlatform/getting-started-java
	* https://cloud.google.com/java/getting-started/hello-world
logging https://cloud.google.com/java/getting-started/logging-application-events
JSP reference http://www.ibm.com/developerworks/java/tutorials/j-introjsp/j-introjsp.html
	[ ] how this is implemented
	[ ] http://scratchblocks.codeclub.org.uk/generator/#project=17088932
web.xml
https://cloud.google.com/appengine/docs/java/config/webxml
Rendering
generate png interfacing with http://scratchblocks.github.io/

Adding external jar library to maven project
http://charlie.cu.cc/2012/06/how-add-external-libraries-maven/

Project layout
https://cloud.google.com/appengine/docs/java/gettingstarted/creating-guestbook




Configure maven goal https://classroom.udacity.com/courses/ud859/lessons/1215898636/concepts/14531090370923#
* appengine:devserver
* appengine:update
